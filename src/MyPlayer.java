import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;
    int moveCounter = 0;
    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 6, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        if(moveCounter == 0){
            bestMove.move = 3;
        }
        moveCounter++;
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0
        int[] onePlayerInRow = new int[4];
        int[] twoPlayerInRow = new int[4];
        double twoblocked=0;
        double twoUnblocked = 0;
        double threeblocked = 0;
        double threeUnblocked =0;
        onePlayerInRow = InRow(gameBoard, 1);
        twoPlayerInRow = InRow(gameBoard, 2);
        if(playerNumber == 1){
            twoblocked = (onePlayerInRow[0]-twoPlayerInRow[0]) * .05;
            twoUnblocked = (onePlayerInRow[1] - twoPlayerInRow[1]) * .17;
            threeblocked = (onePlayerInRow[2] - twoPlayerInRow[2]) * .17;
            threeUnblocked = (onePlayerInRow[3] - twoPlayerInRow[3]) * .6;
        }
        double rowAdvantage =  twoblocked + twoUnblocked + threeblocked + threeUnblocked;
        if(playerNumber == 2){
            rowAdvantage = - rowAdvantage;
        }
        int[] onePlayerInCol = new int[4];
        int[] twoPlayerInCol = new int[4];
        double twocolblocked=0;
        double twocolUnblocked = 0;
        double threecolblocked = 0;
        double threecolUnblocked =0;
        onePlayerInCol = InCol(gameBoard, 1);
        twoPlayerInCol = InCol(gameBoard, 2);
        if(playerNumber == 1){
            twocolblocked = (onePlayerInCol[0]-twoPlayerInCol[0]) * .05;
            twocolUnblocked = (onePlayerInCol[1] - twoPlayerInCol[1]) * .17;
            threecolblocked = (onePlayerInCol[2] - twoPlayerInCol[2]) * .17;
            threecolUnblocked = (onePlayerInCol[3] - twoPlayerInCol[3]) * .6;
        }
        double colAdvantage =  twocolblocked + twocolUnblocked + threecolblocked + threecolUnblocked;
        if(playerNumber == 2){
            colAdvantage = - colAdvantage;
        }
        return  (colAdvantage+ rowAdvantage) /2;
    }

    private int[] InRow (Board board, int player){
        //int array of two_in_row, two_in_row_unblocked, three_in_row, three_in_row_unblocked
        int connectNum = 0;
        int two_in_row = 0;
        int two_in_row_unblocked = 0;
        int three_in_row = 0;
        int three_in_row_unblocked = 0;
        for(int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (board.getBoard()[i][j] == player) {
                    connectNum++;
                } else {
                    if(connectNum == 2) {
                        if (j - connectNum >= 0 && board.getBoard()[i][j - connectNum] == 0) {
                            if (j - connectNum - 1 >= 0 && board.getBoard()[i][j - connectNum - 1] == 0) {
                                two_in_row_unblocked++;  //0011
                            }
                            if (j - connectNum - 1 >= 0 && board.getBoard()[i][j - connectNum - 1] == 1) {
                                three_in_row_unblocked++; //1011
                            } else if (j + 1 < 7 && board.getBoard()[i][j + 1] == 0) {
                                two_in_row_unblocked++; //0110
                            } else {
                                two_in_row++; //011
                            }
                        } else if (j + 1 < 7 && board.getBoard()[i][j + 1] == 0) {
                            if (j + 2 < 7 && board.getBoard()[i][j + 2] == 0) {
                                two_in_row_unblocked++; //1100
                            } else if (j + 2 < 7 && board.getBoard()[i][j + 2] == 1) {
                                three_in_row_unblocked++; //1101
                            } else {
                                two_in_row++; //110
                            }
                        }
                    } else if (connectNum > 2) {
                        if (j - connectNum >= 0 && j + 1 < 7) {
                            if (board.getBoard()[i][j - connectNum] == 0 || board.getBoard()[i][j + 1] == 0) {
                                three_in_row_unblocked++;
                            }
                        } else {
                            three_in_row++;
                        }
                    }
                    connectNum = 0;
                }
            }
        }
        int arr[] = {two_in_row, two_in_row_unblocked, three_in_row, three_in_row_unblocked};
        return arr;
    }

    private int[] InCol (Board board, int player){
        //int array of two_in_row, two_in_row_unblocked, three_in_row, three_in_row_unblocked
        int connectNum = 0;
        int two_in_col = 0;
        int two_in_col_unblocked = 0;
        int three_in_col = 0;
        int three_in_col_unblocked = 0;
        for(int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (board.getBoard()[i][j] == player) {
                    connectNum++;
                } else {
                    if(connectNum == 2) {
                        if (i - connectNum >= 0 && board.getBoard()[i - connectNum][j] == 0) {
                            if (i - connectNum - 1 >= 0 && board.getBoard()[i - connectNum - 1][j] == 0) {
                                two_in_col_unblocked++;  //0011
                            }
                            if (i - connectNum - 1 >= 0 && board.getBoard()[i - connectNum - 1][j] == 1) {
                                three_in_col_unblocked++; //1011
                            } else if (i + 1 < 7 && board.getBoard()[i + 1][j] == 0) {
                                two_in_col_unblocked++; //0110
                            } else {
                                two_in_col++; //011
                            }
                        } else if (i + 1 < 7 && board.getBoard()[i + 1][j] == 0) {
                            if (i + 2 < 7 && board.getBoard()[i + 2][j] == 0) {
                                two_in_col_unblocked++; //1100
                            } else if (i + 2 < 7 && board.getBoard()[i + 2][j] == 1) {
                                three_in_col_unblocked++; //1101
                            } else {
                                two_in_col++; //110
                            }
                        }
                    } else if (connectNum > 2) {
                        if (i - connectNum >= 0 && i + 1 < 7) {
                            if (board.getBoard()[i - connectNum][j] == 0 || board.getBoard()[i + 1][j] == 0) {
                                three_in_col_unblocked++;
                            }
                        } else {
                            three_in_col++;
                        }
                    }
                    connectNum = 0;
                }
            }
        }
        int arr[] = {two_in_col, two_in_col_unblocked, three_in_col, three_in_col_unblocked};
        return arr;
    }

    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value >= max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}
